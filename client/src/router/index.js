import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login.vue"),
    meta: {
      guest: true,
    },
  },
  {
    path: "/register",
    name: "Register",
    component: () => import("../views/Register.vue"),
    meta: {
      guest: true,
    },
  },
  {
    path: "/account",
    name: "Account",
    component: () => import("../views/Account.vue"),
    meta: {
      requireAuth: true,
    },
  },
  {
    path: "/forecasts/:time",
    name: "Forecasts",
    component: () => import("../views/Forecasts.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

// From https://www.digitalocean.com/community/tutorials/how-to-set-up-vue-js-authentication-and-route-handling-using-vue-router
router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requireAuth)) {
    // Handle authenticated routes.
    if (localStorage.getItem("jwt") == null) {
      return next({
        path: "/login",
        params: { nextUrl: to.fullPath },
      });
    }
    let user = JSON.parse(localStorage.getItem("user"));
    if (!to.matched.some((record) => record.meta.is_admin)) {
      return next();
    }
    if (user.is_admin !== 1) {
      return next({ name: "userboard" });
    }
    return next();
  }

  if (!to.matched.some((record) => record.meta.guest)) {
    // Handle non authenticated and non guest routes.
    return next();
  }

  if (localStorage.getItem("jwt") == null) {
    // Handle non authenticated routes.
    return next();
  }

  return next({ name: "userboard" });
});

export default router;
