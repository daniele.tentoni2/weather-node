import { expect } from "chai";
import { shallowMount, createLocalVue } from "@vue/test-utils";
// import VueRouter from 'vue-router';
import Forecasts from "../../src/views/Forecasts.vue";
import WeatherCard from "../../src/components/cards/weather-card.vue";

const localVue = createLocalVue();
// localVue.use(VueRouter)
// const router = new VueRouter()

import { BootstrapVue, BootstrapVueIcons } from "bootstrap-vue";
localVue.use(BootstrapVue);
localVue.use(BootstrapVueIcons);

describe("Forecasts.vue", () => {
  let component;

  beforeEach(() => {
    component = shallowMount(Forecasts, {
      localVue,
      mocks: {
        $route: {
          params: {
            time: "today",
          },
        },
      },
    });
  });

  it("should render WeatherCard on mount", () => {
    expect(component.findComponent(WeatherCard).exists()).to.be.true;
  });
});
