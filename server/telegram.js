/**
 * Telegram bot module.
 *
 * @summary Bot module.
 * @description
 * Handle all messages send to bot. Accept all connections, groups and users
 * too. If groups are going to be too noisy for Heroku free plan, this policy
 * may change.
 *
 * @since 0.0.2
 * @author Daniele Tentoni
 *
 * @todo handle multilanguage (at least italian and english).
 */

const TelegramBot = require("telebot")
const ForecastController = require("./controllers/forecast.controller");

const bot = new TelegramBot({
  token: process.env.TELEGRAM_BOT_TOKEN,
  url: process.env.URL,
  port: 443 // Workaround multi port heroku application.
})

/**
 * Write a log in the server console when a user start the bot.
 */
bot.on("/start", (msg) => console.log("Bot started by %s", msg.from.id));

bot.on("sticker", (msg) => {
  return msg.reply.sticker("http://i.imgur.com/VRYdhuD.png", { asReply: true });
});

bot.on("/help", (msg) => {
  let help = "Ask for meteo informations\n"
  help += "Weather info for a time longer than a week becomes really unreliable, so this bot doesn't show them\n"
  help += "today `city_name`: Print today weather info on specific city\n"
  help += "tomorrow `city_name`: Print tomorrow weather info on specific city\n"
  help += "this week `city_name` \\- Print this week weather info on specific city\n"
  help += "/help: Print help info\n"
  help += "/version: Print version info`n"
  return bot.sendMessage(msg.from.id, help, { parseMode: "MarkdownV2" })
})

bot.on("/version", (msg) => msg.reply.text("0.0.4"));

/**
 * When a user edit a message, bot replay to him.
 */
bot.on("edit", (msg) => {
  return msg.reply.text("I saw it! You edited message!", { asReply: true });
})

/**
 * When a user asks bot to say anything, bot says it.
 */
bot.on(/^\/say (.+)$/, (msg, props) => {
  const text = props.match[1]
  console.log("Say matches: ", props.match)
  return bot.sendMessage(msg.from.id, text, { replyToMessage: msg.message_id })
})

/**
 * Today endpoint.
 * Ask to user to send info for next day?
 * 
 * This rule must match following strings:
 * - /today Cesena, IT (only 2 letters country)
 * / must be the first character in the sentece, followed by today, a space, a city name, a comma and finally a 2 letter country code.
 */
bot.on(/^\/today ([^,]+)[,|\s]\s*(..)$/, async (msg, props) => {
  const city = props.match[1];
  const country = props.match[2];
  try {
    const result = await ForecastController.require_forecast("current", city, country);
    var text = "Weather forecasts for " + city + ", " + country;
    text += "\nSun rise at " + result[0].sunrise + " and set at " + result[0].sunset;
    text += "\nWeather is " + result[0].weather.description + " with " + result[0].clouds + " and " + result[0].precip + " precip.";
    console.log(result);
    return bot.sendMessage(msg.from.id, text, { replyToMessage: msg.message_id });
  } catch (error) {
    bot.sendMessage(msg.from.id, "Error found " + error, { replyToMessage: msg.message_id });
  }
});

/**
 * Tomorrow endpoint.
 * Ask to user to send info for next day?
 */
bot.on(/^\/tomorrow ([^,]+)[,|\s]\s*(..)$/, (msg, props) => {
  const text = props.match[1]
  console.log("Tomorrow matches: ", props.match)
  return bot.sendMessage(msg.from.id, text, { replyToMessage: msg.message_id })
})

/**
 * This week endpoint.
 */
bot.on(/^\/week ([^,]+)[,|\s]\s*(..)$/, (msg, props) => {
  const text = props.match[1]
  console.log("This week matches: ", props.match)
  return bot.sendMessage(msg.from.id, text, { replyToMessage: msg.message_id })
})

if (process.env.NODE_ENV === "production") {
  // Run telegram bot only in production environment.
  bot.start()
}

const sendMessageTo = (id, msg) => {
  if (process.env.NODE_ENV === "production") {
    // Send telegram messages only in production environment.
    bot.sendMessage(id, msg);
  }
}

module.exports = { bot, sendMessageTo }
