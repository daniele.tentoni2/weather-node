/**
 * Todo's model.
 *
 * Base todo model. Need improvement on data validations.
 *
 * @author Daniele Tentoni
 * @since 0.0.3
 */

const mongoose = require("mongoose")

const todoSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  // Must be set by default to false.
  completed: {
    type: Boolean,
    required: [true, "Todo must be completed or not."]
  }
}, {
  collection: "todos"
})

todoSchema.query.byCompleted = function (completed) {
  return this.where({ completed })
}

module.exports = mongoose.model("todo", todoSchema)
