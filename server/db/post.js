/**
 * Post's model.
 *
 * Base post model. Need improvement on data validations.
 *
 * @author Daniele Tentoni
 * @since 0.0.3
 */

const mongoose = require("mongoose")

const postSchema = new mongoose.Schema({
  // Post title. This is required.
  title: {
    type: String,
    required: true
  },
  // Post content. This is not required. A user may post nothing.
  content: {
    type: String,
    required: false
  },
  // Post publish date.
  publishedAt: {
    default: Date.now,
    required: true,
    type: Date
  }
}, {
  collection: "posts"
})

// Exports the postSchema for use elsewhere.
module.exports = mongoose.model("post", postSchema)
