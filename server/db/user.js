/**
 * User's model.
 *
 * Base user model. Need improvement on data validations.
 *
 * @author Daniele Tentoni
 * @since 0.0.3
 */

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  confirmed: {
    type: Boolean,
    default: false
  },
  preferredCity: {
    type: String
  },
  preferredCountry: {
    type: String
  },
  preferredService: {
    type: String,
    enum: ["weatherBit", "OpenWeather"]
  }
}, {
  collection: "users"
});

userSchema.query.byConfirmed = (byConfirmed) => this.where({ byConfirmed });

module.exports = mongoose.model("user", userSchema);