/**
 * Forecast model.
 *
 * Base forecast model. Need improvement on data validations.
 * Consider to use a complex id based on the required city and date to improve
 * performance. Can become indexes?
 *
 * @author Daniele Tentoni
 * @since 0.0.3
 */

const mongoose = require("mongoose");

const forecastSchema = new mongoose.Schema({
  // City of the forecast. This can be used for caching.
  city: {
    type: String,
    required: [true, "You need to set a city for a forecast"]
  },
  // Country, required for following apis: weatherbit.io (2 letter)
  country: {
    type: String,
    required: [true, "You need to set a country for your forecase"]
  },
  // Postal code. One from [postal_code, city_id, coordinates] are required.
  postal_code: {
    type: Number,
    required: false
  },
  // Level of clouds for weatherbit.io
  clouds: {
    type: Number,
    required: false
  },
  // Temperature - Default (C) from weatherbit.io
  temp: {
    type: Number
  },
  // Precipitation from weatherbit.io
  precip: {
    type: Number
  },
  // Date of the forecast. This is not the date of the request.
  date: {
    type: Date,
    required: true
  },
  // The value can be false while the requests are made to other sites.
  value: {
    type: String,
    required: false
  }
}, {
  collection: "forecasts"
});

module.exports = mongoose.model("forecast", forecastSchema);

/*
Mongoose schema for weatherbit.io forecast
{
  "rh": {
    "type": "Number"
  },
  "pod": {
    "type": "String"
  },
  "lon": {
    "type": "Number"
  },
  "pres": {
    "type": "Number"
  },
  "timezone": {
    "type": "String"
  },
  "ob_time": {
    "type": "Date"
  },
  "country_code": {
    "type": "String"
  },
  "clouds": {
    "type": "Number"
  },
  "ts": {
    "type": "Number"
  },
  "solar_rad": {
    "type": "Number"
  },
  "state_code": {
    "type": "String"
  },
  "city_name": {
    "type": "String"
  },
  "wind_spd": {
    "type": "Number"
  },
  "wind_cdir_full": {
    "type": "String"
  },
  "wind_cdir": {
    "type": "String"
  },
  "slp": {
    "type": "Number"
  },
  "vis": {
    "type": "Number"
  },
  "h_angle": {
    "type": "Number"
  },
  "sunset": {
    "type": "String"
  },
  "dni": {
    "type": "Number"
  },
  "dewpt": {
    "type": "Number"
  },
  "snow": {
    "type": "Number"
  },
  "uv": {
    "type": "Number"
  },
  "precip": {
    "type": "Number"
  },
  "wind_dir": {
    "type": "Number"
  },
  "sunrise": {
    "type": "String"
  },
  "ghi": {
    "type": "Number"
  },
  "dhi": {
    "type": "Number"
  },
  "aqi": {
    "type": "Number"
  },
  "lat": {
    "type": "Number"
  },
  "weather": {
    "icon": {
      "type": "String"
    },
    "code": {
      "type": "Number"
    },
    "description": {
      "type": "String"
    }
  },
  "datetime": {
    "type": "String"
  },
  "temp": {
    "type": "Number"
  },
  "station": {
    "type": "String"
  },
  "elev_angle": {
    "type": "Number"
  },
  "app_temp": {
    "type": "Number"
  }
}
*/