/**
 * Forecast's routes.
 * 
 * @description Group in this file all calls for forecasts. This may produce
 * addictional traffic outgoing to other weather api sites.
 *
 * @author Daniele Tentoni
 * @since 0.0.3
 */

const express = require("express");
const ForecastRoute = express.Router();
const Forecast = require("../db/forecast");
const ForecastController = require("../controllers/forecast.controller");

ForecastRoute.route("/forecasts").get((req, res, next) => {
  var all = Forecast.find();
  all.exec((error, data) => {
    if (error) {
      return next(error);
    }
    return res.status(200).json(data);
  });
});

ForecastRoute.route("/forecast/:time").get(ForecastController.require_forecast);

module.exports = ForecastRoute;