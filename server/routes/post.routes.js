const express = require("express");
const PostRoute = express.Router();

// Post model.
const Post = require("../db/post");

PostRoute.route("/posts").get((req, res, next) => {
  Post.find((error, data) => {
    if (error) {
      return next(error.errors);
    }
    return res.status(200).json(data);
  });
});

PostRoute.route("/post/:id").get((req, res, next) => {
  if (typeof req.params.id !== "number") {
    return next({ description: "Invalid input type: id must be a number" });
  }

  Post.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error.errors);
    }
    return res.status(200).json(data);
  });
});

PostRoute.route("/post").post((req, res, next) => {
  if (
    typeof req.body.title === "undefined" ||
    typeof req.body.title !== "string"
  ) {
    return next({ description: "Invalid input type: title must be defined" });
  }

  const newPost = new Post({
    title: req.body.title,
  });
  newPost.save((error, data) => {
    if (error) {
      return next(error.errors);
    }
    return res.status(200).json(data);
  });
});

PostRoute.route("/post/:id").put((req, res, next) => {
  if (typeof req.params.id !== "number") {
    return next({ description: "Invalid input type: id must be a number" });
  }

  Post.findByIdAndUpdate(
    req.params.id,
    { $set: req.body },
    { new: true },
    (error, data) => {
      if (error) {
        return next(error.errors);
      }
      return res.status(200).json(data);
    }
  );
});

PostRoute.route("/post/:id").delete((req, res, next) => {
  if (typeof req.params.id !== "number") {
    return next({ description: "Invalid input type: id must be a number" });
  }

  Post.findByIdAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error.errors);
    }
    return res.status(200).json(data);
  });
});

module.exports = PostRoute;
