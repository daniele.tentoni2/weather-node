/**
 * User's routes.
 *
 * TODO: Doesn't return user's passwords.
 *
 * @author Daniele Tentoni
 * @since 0.0.3
 */

const express = require("express");
const UserRoute = express.Router();
const User = require("../db/user");

UserRoute.route("/users").get((req, res, next) => {
  var all = User.find();
  if (typeof req.query.confirmed !== "undefined") {
    all = all.where({ confirmed: req.query.confirmed });
  }
  all.exec((error, data) => {
    if (error) {
      return next(error);
    }
    return res.status(200).json(data);
  });
});

UserRoute.route("/user").post((req, res, next) => {
  if (typeof req.body === "undefined") {
    return next({ description: "Body must be defined." });
  }
  // This avoid forcing fields.
  const newUser = new User({
    name: req.body.name,
    password: req.body.password,
    email: req.body.email,
    preferredCity: req.body.preferredCity
  });
  newUser.save((error, data) => {
    if (error) {
      return next(error);
    }
    return res.status(200).json(data);
  });
});

UserRoute.route("/user/:id").get((req, res, next) => {
  User.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    }
    return res.status(200).json(data);
  });
});

UserRoute.route("/user/:id").put((req, res, next) => {
  // TODO: Manage edit of other fields.
  User.findByIdAndUpdate(req.params.id, { confirmed: true }, (error, data) => {
    if (error) {
      return next(error);
    }
    return res.status(200).json(data);
  });
});

UserRoute.route("/user/:id").delete((req, res, next) => {
  User.findByIdAndDelete(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    }
    return res.status(200).json(data);
  });
});

module.exports = UserRoute;