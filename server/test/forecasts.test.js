/**
 * Forecasts Test Script.
 * 
 * @summary Test forecasts script api endpoints.
 * @author Daniele Tentoni
 * @since 0.0.4
 */

const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../index");

chai.should();
chai.use(chaiHttp);

describe("Forecasts", () => {
  it("should return a correct status code", async () => {
    const result = await chai.request(server)
    .get("/api/forecast/current?city=Cesena&country=IT")
    .set("content-type", "application/json")
    .send();
    result.should.have.status(200);
    result.body.should.be.a("array");
    result.body.length.should.be.eql(1);
    const item = result.body[0];
    item.should.have.property("timezone").eql("Europe/Rome");
    item.should.have.property("country_code").eql("IT");
    item.should.have.property("city_name").eql("Cesena");
  });
});