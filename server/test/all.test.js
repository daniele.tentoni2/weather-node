/**
 * Base Test Script.
 *
 * @author Daniele Tentoni
 * @since 0.0.3
 */

const chai = require("chai")
const chaiHttp = require("chai-http")
const server = require("../index")

chai.should()
chai.use(chaiHttp)

describe("Add method", () => {
  it("should connect to the server", async () => {
    const added = await chai.request(server)
      .post("/api/add")
      .set("Connection", "keep alive")
      .set("content-type", "application/json")
      .type("form")
      .send({
        firstNumber: 2,
        secondNumber: 3
      });
    added.should.have.status(200);
    added.body.should.be.a("object");
    added.body.result.should.be.eql(5);
  });
});

describe("Test regular expressions", () => {
  it("should match city and country expression", async () => {
    const expression = /^\/today ([^,]+)[,|\s]\s*(..)$/;
    const tester = "/today Cesena, IT";
    const matches = expression.exec(tester);
    matches.length.should.be.eql(3);
    matches[0].should.be.eql(tester);
    matches[1].should.be.eql("Cesena");
    matches[2].should.be.eql("IT");
  });
});