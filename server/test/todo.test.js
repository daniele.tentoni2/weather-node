/**
 * Todo Test Script.
 *
 * @summary Test todo script api endpoint on mongodb schema.
 * @author Daniele Tentoni
 * @since 0.0.3
 */

const chai = require("chai")
const chaiHttp = require("chai-http")
const server = require("../index")

chai.should()
chai.use(chaiHttp)

// No Mongodb connection available in local scope.
describe("Todo apis", () => {
  it("shouldn't contains todos", async () => {
    const completed = await chai.request(server)
      .get("/todo/completed")
      .set("connection", "keep alive")
      .set("content-type", "application/json")
      .send();
    completed.should.have.status(200);
    completed.body.should.be.a("object");
    completed.body.should.have.property("message");
  });
  it("should get a todo", async () => {
    const todo = await chai.request(server)
      .get("/api/todo/6042a5d2d296660015b5754e")
      .set("connection", "keep alive")
      .set("content-type", "application/json")
      .send();
    todo.should.have.status(200);
    todo.body.should.be.a("object");
    todo.body.title.should.be.eql("Cose da fare");
  });
});