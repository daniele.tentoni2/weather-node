/**
 * Regex utils functions.
 *
 * Use this file to store many regex checks to retreive in other files.
 *
 * @author Daniele Tentoni
 * @since 0.0.4
 */

/**
 * Validate with a regex the given country code.
 * @param {String} country Two letter country code for forecasts.
 * @returns true if is a valid code, false otherwise.
 */
exports.check_country = (country) => /^[A-Z]{2}$/.test(country);
