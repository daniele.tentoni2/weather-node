/**
 * Weatherbit Forecast Controller.
 *
 * Execute calls to Weatherbit apis in order to get forecasts.
 *
 * @author Daniele Tentoni
 * @since 0.0.4
 */

const axios = require("axios");

const require_forecast = async (time, city, country) => {
  if (
    typeof process.env.WEATHERBIT_URL === "undefined" ||
    typeof process.env.WEATHERBIT_KEY === "undefined"
  ) {
    return { description: "You need to set environment variables" };
  }

  try {
    const response = await axios_request(time, city, country);
    console.log("Axios success: %s", response);
    return response.data;
  } catch (error) {
    console.error("Axios error:%s", error);
    return error;
  }
};

const axios_request = async (time, city, country) => {
  const weatherbit_url = parseTimeToUrl(process.env.WEATHERBIT_URL, time);
  try {
    const body = await axios.get(weatherbit_url, {
      params: {
        city,
        country,
        key: process.env.WEATHERBIT_KEY,
      },
    });
    console.log("Response received %s(%s)", body.statusText, body.status);
    return body.data;
  } catch (err) {
    if (err.response) {
      // The server responded with a error status code.
      const message =
        "Response error: " +
        err.response.statusText +
        "(" +
        err.response.status +
        ") to request: " +
        time;
      console.error(message);
      console.error(
        "Headers: %o => %s",
        err.response.headers,
        err.response.data
      );
      return message;
    } else if (err.request) {
      // The request was made but no response was received,
      const message = "Error error without response: " + err.request;
      console.error(message);
      return message;
    } else {
      // Something happened in setting up the request that triggered an Error
      const message = "Error setting up the request: " + err.message;
      console.error(message);
      return message;
    }
  }
};

/**
 * Parse arguments to a well formed url to call for weather forecasts.
 * @param {string} url base url of weather service.
 * @param {string} time time to forecast.
 * @returns composed url.
 */
const parseTimeToUrl = (url, time) => {
  if (typeof time === "string" && time === "current") {
    return url + "/current";
  }
  return url;
};

module.exports = { require_forecast };
