module.exports = {
  env: {
    browser: true,
    es6: true,
    mocha: true,
    node: true
  },
  extends: [
    "eslint:recommended",
  ],
  parserOptions: {
    ecmaVersion: 2017
  },
  rules: {
    quotes: ["error", "double"]
  }
}
