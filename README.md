# weather-node

Weather aggregatation. University project for ASW course. Made with MEVN stack.

Visit the application to `https://dt-weather-mevn.herokuapp.com`.

Join bot on [Telegram](http://t.me/NodeWeatherBot) and the [official group](https://t.me/joinchat/dQupy4_hL7s2ZDY8).

## Prepare application

Remember to add env variables like the `.env.example` file.
1. Create a `.env` file
2. Ignore it in your `.gitignore` file
3. Use your editor to edit `.env` file
4. Read the `.env` file with the dotenv npm package `npm i --save-dev dotenv` as a dev dependency
5. Use the preloading option of dotenv to remove any runtime references to it
6. Use npm scripts to run your node app
7. Create a template file for your variables called `.env.example`
Thanks to [John Papa](https://medium.com/the-node-js-collection/making-your-node-js-work-everywhere-with-environment-variables-2da8cdf6e786) for this useful guide.

## Run server

To run locally, use `npm run start_locally` instead of `npm run start`.

Create posts with `https://dt-weather-mean.herokuapp.com/api/post`.